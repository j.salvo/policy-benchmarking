# Policy Benchmarking

This project consists of home-spun Policy Benchmarking Report. It was developed to streamline the reporting processes and enhance data analysis for Risk International. The system leverages custom R-coded charts and tables to mirror industry-standard reports and provides dynamic management and time-tracking tools for internal use.

## Advisen Alternative

Advisen, a former industry leader in policy data collection, no longer collects policy data as they once did. At Risk International, I led the initiative to build a comprehensive policy database that captures data since 1986.

### Key Features:
- **Policy Fact Table**: The core of the database is a fact table for policies, which is linked to various other dimension tables to support complex analyses.
- **Custom R Reports**: Built R-coded charts and tables to mimic the structure and insights of Advisen's original reports.
- **Benchmarking Analysis**: Premium, limit, and deductible distributions in our dataset show consistency with Advisen’s historical dataset. In certain lines of business (see the `Drill_Down` image in the GitLab project), we now have more recent policy data to benchmark against compared to Advisen’s dataset.
